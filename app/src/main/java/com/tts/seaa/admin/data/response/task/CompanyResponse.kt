package com.tts.seaa.admin.data.response.task

import com.google.gson.annotations.SerializedName

data class CompanyResponse(

    @field:SerializedName("result")
    val result: List<CompanyResultItem?>? = null,

    @field:SerializedName("key")
    val key: String? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class CompanyResultItem(

    @field:SerializedName("com_type")
    val comType: String? = null,

    @field:SerializedName("com_email")
    val comEmail: String? = null,

    @field:SerializedName("com_id")
    val comId: String? = null,

    @field:SerializedName("com_phone")
    val comPhone: String? = null,

    @field:SerializedName("firm_id")
    val firmId: String? = null,

    @field:SerializedName("com_name")
    val comName: String? = null
)
