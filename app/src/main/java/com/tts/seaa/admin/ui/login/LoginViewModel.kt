package com.tts.seaa.admin.ui.login

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.tts.seaa.admin.data.preferences.PreferenceProvider
import com.tts.seaa.admin.data.response.firmkey.FirmKeyRes
import com.tts.seaa.admin.data.response.login.LoginRes
import com.tts.seaa.admin.utils.Coroutines

class LoginViewModel(
    private val loginRepo: LoginRepo,
    private val preferenceProvider: PreferenceProvider,
    private val context: Context
) : ViewModel() {

    private var _resultFirmKey: MutableLiveData<FirmKeyRes> = MutableLiveData<FirmKeyRes>()
    private var _resultMobileNo: MutableLiveData<LoginRes> = MutableLiveData<LoginRes>()
    private var _resultOTP: MutableLiveData<LoginRes> = MutableLiveData<LoginRes>()

    var firmIDLiveData: LiveData<FirmKeyRes> = _resultFirmKey
    var mobileNoLiveData: LiveData<LoginRes> = _resultMobileNo
    var OTPLiveData: LiveData<LoginRes> = _resultOTP

    fun getFirmKey(firmId: String) {

        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty("firm_id", firmId)
                val tasks = loginRepo.getFirmKey(jsonObject)
                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.message == null) {
                        tasks.body()!!.result?.refKey?.let { preferenceProvider.saveFirmKey(it) }
                        _resultFirmKey.value = tasks.body()
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun sendMobileNo(mobileNo: String) {

        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                jsonObject.addProperty("firm_mobile", mobileNo)

                val tasks = loginRepo.sendMobileNo(jsonObject)
                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.message == "success") {
                        _resultMobileNo.value = tasks.body()
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun verifyOTP(otp: String) {

        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                jsonObject.addProperty("firm_otp", otp)

                val tasks = loginRepo.verifyOTP(jsonObject)
                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.message == "success") {
                        _resultOTP.value = tasks.body()
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }
}