package com.tts.seaa.admin.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tts.seaa.admin.R
import com.tts.seaa.admin.data.api.ApiInterface
import com.tts.seaa.admin.data.api.RetrofitClient
import com.tts.seaa.admin.data.preferences.PreferenceProvider
import kotlinx.android.synthetic.main.activity_firm.*

class FirmActivity : AppCompatActivity() {

    lateinit var preferenceProvider: PreferenceProvider
    lateinit var loginRepo: LoginRepo
    lateinit var api: ApiInterface
    private lateinit var loginViewModel: LoginViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firm)

        api = RetrofitClient(this).getClient()
        loginRepo = LoginRepo(api)
        preferenceProvider = PreferenceProvider(this)

        loginViewModel = getViewModel()

        btnSubmitFirmId.setOnClickListener {
            if (extFirmId.text.toString() == "") {
                extFirmId.error = "Please Enter FirmID"
            } else {
                loginViewModel.getFirmKey(extFirmId.text.toString())
            }
        }

        loginViewModel.firmIDLiveData.observe(this, Observer {
            if (it.result != null) {
                Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show()
                val i = Intent(this, LoginActivity::class.java)
                startActivity(i)
            }

        })

    }


    private fun getViewModel(): LoginViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return LoginViewModel(loginRepo, preferenceProvider, this@FirmActivity) as T
            }
        })[LoginViewModel::class.java]

    }
}