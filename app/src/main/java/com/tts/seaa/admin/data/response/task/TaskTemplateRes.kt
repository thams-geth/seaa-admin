package com.tts.seaa.admin.data.response.task

import com.google.gson.annotations.SerializedName

data class TaskTemplateRes(

	@field:SerializedName("result")
	val result: List<TaskTemplateResultItem?>? = null,

	@field:SerializedName("key")
	val key: String? = null,
	val message: String? = null

)

data class TaskTemplateResultItem(

	@field:SerializedName("tmp_act_name")
	val tmpActName: String? = null,

	@field:SerializedName("tem_act_status")
	val temActStatus: TemActStatus? = null,

	@field:SerializedName("tmp_id")
	val tmpId: String? = null,

	@field:SerializedName("tmp_act_id")
	val tmpActId: String? = null,

	@field:SerializedName("firm_id")
	val firmId: String? = null,

	@field:SerializedName("tmp_act_order")
	val tmpActOrder: String? = null
)

data class TemActStatus(

	@field:SerializedName("act_id")
	val actId: String? = null,

	@field:SerializedName("act_date_added")
	val actDateAdded: String? = null,

	@field:SerializedName("act_task_status")
	val actTaskStatus: String? = null
)
