package com.tts.seaa.admin.ui.task

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tts.seaa.admin.R
import com.tts.seaa.admin.data.api.ApiInterface
import com.tts.seaa.admin.data.api.RetrofitClient
import com.tts.seaa.admin.data.preferences.PreferenceProvider
import com.tts.seaa.admin.data.response.employee.EmpCurrentTask
import com.tts.seaa.admin.data.response.employee.EmployeeResponseResultItem
import com.tts.seaa.admin.data.response.task.AllTaskResponseResultItem
import com.tts.seaa.admin.data.response.task.CompanyResultItem
import com.tts.seaa.admin.databinding.FragmentTaskBinding
import com.tts.seaa.admin.ui.FragmentClickListener
import com.tts.seaa.admin.ui.home.HomeRepository
import com.tts.seaa.admin.ui.home.HomeViewModel
import kotlinx.android.synthetic.main.fragment_task.*


class TaskFragment : Fragment(), AdapterView.OnItemSelectedListener {

    lateinit var fragmentClickListener: FragmentClickListener


    companion object {
        fun newInstance() = TaskFragment()
    }

    private lateinit var con: Context


    lateinit var preferenceProvider: PreferenceProvider
    lateinit var homeRepository: HomeRepository
    lateinit var api: ApiInterface


    private lateinit var binding: FragmentTaskBinding
    private lateinit var viewModel: HomeViewModel

    private var companyResultItem = ArrayList<CompanyResultItem>()
    private var companyResultItemName = ArrayList<String>()


    private var employeeResultItem = ArrayList<EmployeeResponseResultItem>()
    private var employeeResultItemName = ArrayList<String>()


    override fun onAttach(context: Context) {
        super.onAttach(context)
        con = context
        fragmentClickListener = con as FragmentClickListener

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_task, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(TaskViewModel::class.java)


        api = RetrofitClient(con).getClient()
        homeRepository = HomeRepository(api)
        preferenceProvider = PreferenceProvider(con)


        viewModel = getViewModel()

        getALLTask()
        viewModel.getCompanies()
        viewModel.getFirmEmployees()

        viewModel.resultCompany.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                companyResultItem.add(CompanyResultItem("", "", "", "", "", ""))
                companyResultItemName.add("Select")
                for (i in it.result!!) {
                    companyResultItem.add(i!!)
                    companyResultItemName.add(i.comName!!)
                }
            }
            val ad: ArrayAdapter<String> = ArrayAdapter(requireContext(), R.layout.item_spinner, companyResultItemName)
            ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spnCompany.adapter = ad
            binding.spnCompany.onItemSelectedListener = this
        })

        viewModel.resultEmployee.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                employeeResultItem.add(EmployeeResponseResultItem("", 0, "", "", "", 0, 0, EmpCurrentTask("", ""), "", ""))
                employeeResultItemName.add("Select")

                for (i in it) {
                    employeeResultItem.add(i)
                    employeeResultItemName.add(i.empName!!)
                }
            }
            val ad: ArrayAdapter<String> = ArrayAdapter(requireContext(), R.layout.item_spinner, employeeResultItemName)
            ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spnEmployee.adapter = ad
            binding.spnEmployee.onItemSelectedListener = this
        })


        viewModel.resultAllTask.observe(viewLifecycleOwner, Observer {

            var list = ArrayList<AllTaskResponseResultItem>()
            if (it != null) {
                for (i in it.result!!) {
                    list.add(i!!)
                }
            }

            val homeAdapter = TaskListAdapter(list, con, con as FragmentClickListener)
            binding.recyclerView.layoutManager = LinearLayoutManager(con)
            binding.recyclerView.adapter = homeAdapter


        })

        binding.drawerIcon.setOnClickListener {
            fragmentClickListener.drawerClick()
        }
        binding.btnAddTask.setOnClickListener {
            fragmentClickListener.onClick("AddActivity")
        }

    }

    private fun getALLTask() {
        viewModel.getAllTask(
            if (spnEmployee.selectedItemPosition > 0) spnEmployee.selectedItem.toString() else "",
            if (spnCompany.selectedItemPosition > 0) spnCompany.selectedItem.toString() else "",
            if (spnStatus.selectedItemPosition > 0) spnStatus.selectedItem.toString() else ""
        )
    }

    private fun getViewModel(): HomeViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return HomeViewModel(homeRepository, preferenceProvider, con) as T
            }
        })[HomeViewModel::class.java]
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent!!.id) {
            R.id.spnCompany -> if (position > 0) getALLTask()
            R.id.spnEmployee -> if (position > 0) getALLTask()
            R.id.spnStatus -> if (position > 0) getALLTask()

        }
    }


    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

}