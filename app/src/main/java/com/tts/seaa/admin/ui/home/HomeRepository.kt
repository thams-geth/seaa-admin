package com.tts.seaa.admin.ui.home

import com.google.gson.JsonObject
import com.tts.seaa.admin.data.api.ApiInterface
import com.tts.seaa.admin.data.response.employee.*
import com.tts.seaa.admin.data.response.task.*
import retrofit2.Response


class HomeRepository(private val api: ApiInterface) {

    suspend fun getEmployee(jsonObject: JsonObject): Response<EmployeeResponse> {
        return api.getEmployees(jsonObject)
    }

    suspend fun getEmployeeDetails(jsonObject: JsonObject): Response<EmployeeDetailResponse> {
        return api.getEmployeeDetails(jsonObject)
    }

    suspend fun getEmployeeTaskList(jsonObject: JsonObject): Response<EmployeeTaskListRes> {
        return api.getEmployeeTaskList(jsonObject)
    }

    suspend fun getEmployeeTask(jsonObject: JsonObject): Response<EmployeeTaskResponse> {
        return api.getEmployeeTask(jsonObject)
    }

    suspend fun getEmployeeTaskActivities(jsonObject: JsonObject): Response<EmployeeTaskActivityRes> {
        return api.getEmployeeTaskActivities(jsonObject)
    }

    suspend fun getEmployeeActivity(jsonObject: JsonObject): Response<EmployeeActivityResponse> {
        return api.getEmployeeActivities(jsonObject)
    }

    suspend fun getAllTasks(jsonObject: JsonObject): Response<AllTaskResponse> {
        return api.getAllTask(jsonObject)
    }

    suspend fun getTaskByDate(jsonObject: JsonObject): Response<TaskByDateResponse> {
        return api.getTaskByDate(jsonObject)
    }

    suspend fun getTeams(jsonObject: JsonObject): Response<TeamResponse> {
        return api.getTeams(jsonObject)
    }

    suspend fun getCompanies(jsonObject: JsonObject): Response<CompanyResponse> {
        return api.getCompanies(jsonObject)
    }

    suspend fun getTemplate(jsonObject: JsonObject): Response<TemplateResponse> {
        return api.getTemplate(jsonObject)
    }

    suspend fun addTask(jsonObject: JsonObject): Response<AddTaskResponse> {
        return api.addTask(jsonObject)
    }

    suspend fun getTaskDetails(jsonObject: JsonObject): Response<TaskDetailResponse> {
        return api.getTaskDetails(jsonObject)
    }

    suspend fun getTaskTemplatesActivities(jsonObject: JsonObject): Response<TaskTemplateRes> {
        return api.getTaskTemplatesActivities(jsonObject)
    }

    suspend fun getTaskSubActivities(jsonObject: JsonObject): Response<TaskSubRes> {
        return api.getTaskSubActivities(jsonObject)
    }


}