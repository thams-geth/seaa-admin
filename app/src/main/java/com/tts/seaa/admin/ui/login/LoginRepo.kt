package com.tts.seaa.admin.ui.login

import com.google.gson.JsonObject
import com.tts.seaa.admin.data.api.ApiInterface
import com.tts.seaa.admin.data.response.firmkey.FirmKeyRes
import com.tts.seaa.admin.data.response.login.LoginRes
import retrofit2.Response

class LoginRepo(private val api: ApiInterface) {

    suspend fun getFirmKey(jsonObject: JsonObject): Response<FirmKeyRes> {
        return api.getFirmKey(jsonObject)
    }

    suspend fun sendMobileNo(jsonObject: JsonObject): Response<LoginRes> {
        return api.sendMobileNo(jsonObject)
    }

    suspend fun verifyOTP(jsonObject: JsonObject): Response<LoginRes> {
        return api.verifyOTP(jsonObject)
    }
}