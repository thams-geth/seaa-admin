package com.tts.seaa.admin.data.response.employee

import com.google.gson.annotations.SerializedName

data class EmployeeTaskListRes(

	@field:SerializedName("result")
	val result: List<EmployeeTaskListResultItem?>? = null,

	@field:SerializedName("key")
	val key: String? = null,
	val message: String? = null

)

data class EmployeeTaskListResultItem(

	@field:SerializedName("due_date")
	val dueDate: String? = null,

	@field:SerializedName("task_title")
	val taskTitle: String? = null,

	@field:SerializedName("Task_id")
	val taskId: String? = null,

	@field:SerializedName("emp_id")
	val empId: String? = null,

	@field:SerializedName("completed_date")
	val completedDate: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
