package com.tts.seaa.admin.ui.doc

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.tts.seaa.admin.R
import com.tts.seaa.admin.databinding.FragmentDocBinding
import com.tts.seaa.admin.ui.FragmentClickListener
import com.tts.seaa.admin.utils.getTodayDateHeader

class DocFragment : Fragment() {


    lateinit var fragmentClickListener: FragmentClickListener

    companion object {
        fun newInstance() = DocFragment()
    }

    private lateinit var binding: FragmentDocBinding
    private lateinit var viewModel: DocViewModel


    private lateinit var con: Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        con = context
        fragmentClickListener = con as FragmentClickListener
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_doc, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DocViewModel::class.java)




        binding.btnUploadNew.setOnClickListener {
            fragmentClickListener.onClick("UploadDoc")
        }
        binding.drawerIcon.setOnClickListener {
            fragmentClickListener.drawerClick()
        }
    }

}