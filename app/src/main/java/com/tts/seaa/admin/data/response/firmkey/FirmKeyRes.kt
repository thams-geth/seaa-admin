package com.tts.seaa.admin.data.response.firmkey

data class FirmKeyRes(
    val key: String? = null,
    val message: String? = null,
    val result: FirmResult? = null
)