package com.tts.seaa.admin.ui

interface FragmentClickListener {
    fun onClick(fragment: String)
    fun onClickWithEmpId(fragment: String, emp_id: String)
    fun onClickWithEmpIdTaskID(fragment: String, emp_id: String, task_id: String)
    fun onBack()
    fun drawerClick()

}