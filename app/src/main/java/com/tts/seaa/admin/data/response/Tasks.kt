package com.tts.seaa.admin.data.response

data class Tasks(
    val key: String,
    val message: String,
    val result: ArrayList<Result>
)