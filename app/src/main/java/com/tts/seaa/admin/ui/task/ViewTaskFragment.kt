package com.tts.seaa.admin.ui.task

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tts.seaa.admin.R
import com.tts.seaa.admin.data.api.ApiInterface
import com.tts.seaa.admin.data.api.RetrofitClient
import com.tts.seaa.admin.data.preferences.PreferenceProvider
import com.tts.seaa.admin.data.response.employee.EmployeeTaskActivityResultItem
import com.tts.seaa.admin.data.response.task.TaskTemplateResultItem
import com.tts.seaa.admin.databinding.FragmentViewTaskBinding
import com.tts.seaa.admin.ui.FragmentClickListener
import com.tts.seaa.admin.ui.home.HomeRepository
import com.tts.seaa.admin.ui.home.HomeViewModel
import com.tts.seaa.admin.utils.Constants


class ViewTaskFragment : Fragment() {

    lateinit var fragmentClickListener: FragmentClickListener
    private var empId: String? = null
    private var taskId: String? = null


    companion object {
        fun newInstance(emp_id: String, task_id: String) = ViewTaskFragment().apply {
            arguments = Bundle().apply {
                putString(Constants.ARG_EMP_ID, emp_id)
                putString(Constants.ARG_TASK_ID, task_id)
            }
        }
    }

    private lateinit var con: Context


    lateinit var preferenceProvider: PreferenceProvider
    lateinit var homeRepository: HomeRepository
    lateinit var api: ApiInterface


    private lateinit var binding: FragmentViewTaskBinding
    private lateinit var viewModel: HomeViewModel


    override fun onAttach(context: Context) {
        super.onAttach(context)
        con = context
        fragmentClickListener = con as FragmentClickListener

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            empId = it.getString(Constants.ARG_EMP_ID)
            taskId = it.getString(Constants.ARG_TASK_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_view_task, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(TaskViewModel::class.java)


        api = RetrofitClient(con).getClient()
        homeRepository = HomeRepository(api)
        preferenceProvider = PreferenceProvider(con)


        viewModel = getViewModel()
//        viewModel.getEmployeeTask(empId!!, taskId!!)
//        viewModel.getEmployeeTaskActivities(empId!!, taskId!!)

        viewModel.getTaskDetails(taskId!!)


//        viewModel.resultEmployeeTaskActivity.observe(viewLifecycleOwner, Observer {
//            var list = ArrayList<EmployeeTaskActivityResultItem>()
//            if (it != null) {
//                for (i in it.result!!) {
//                    list.add(i!!)
//                }
//            }
//            val homeAdapter = EmployeeActivityAdapter(list, con, con as FragmentClickListener)
//            binding.recyclerView.layoutManager = LinearLayoutManager(con)
//            binding.recyclerView.adapter = homeAdapter
//
//        })

//        viewModel.resultEmployeeTask.observe(viewLifecycleOwner, Observer {
//            if (it.result != null)
//                binding.taskDetails = it.result!!
//        })

        viewModel.resultTaskDetail.observe(viewLifecycleOwner, Observer {
            if (it.result != null) {
                binding.taskDetails = it.result[0]
                viewModel.getTaskTemplatesActivities(taskId!!, it.result[0]!!.template!!)
            }

        })

        viewModel.resultTaskTemplate.observe(viewLifecycleOwner, Observer {
            var list = ArrayList<TaskTemplateResultItem>()
            if (it != null) {
                for (i in it.result!!) {
                    list.add(i!!)
                }
            }
            val homeAdapter = TaskTemplateListAdapter(list, con, con as FragmentClickListener)
            binding.recyclerView.layoutManager = LinearLayoutManager(con)
            binding.recyclerView.adapter = homeAdapter

        })


        binding.drawerIcon.setOnClickListener {
            fragmentClickListener.drawerClick()
        }

    }

    private fun getViewModel(): HomeViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return HomeViewModel(homeRepository, preferenceProvider, con) as T
            }
        })[HomeViewModel::class.java]
    }

}