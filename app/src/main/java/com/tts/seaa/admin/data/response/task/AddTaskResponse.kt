package com.tts.seaa.admin.data.response.task

import com.google.gson.annotations.SerializedName

data class AddTaskResponse(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("key")
	val key: String? = null
)
