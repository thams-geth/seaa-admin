package com.tts.seaa.admin.ui.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tts.seaa.admin.R
import com.tts.seaa.admin.data.api.ApiInterface
import com.tts.seaa.admin.data.api.RetrofitClient
import com.tts.seaa.admin.data.preferences.PreferenceProvider
import com.tts.seaa.admin.data.response.employee.EmployeeResponseResultItem
import com.tts.seaa.admin.databinding.FragmentHomeBinding
import com.tts.seaa.admin.ui.FragmentClickListener
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {


    lateinit var fragmentClickListener: FragmentClickListener
    lateinit var preferenceProvider: PreferenceProvider
    lateinit var homeRepository: HomeRepository
    lateinit var api: ApiInterface
    private lateinit var viewModel: HomeViewModel
    lateinit var binding: FragmentHomeBinding

    private lateinit var con: Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        con = context
        fragmentClickListener = con as FragmentClickListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        api = RetrofitClient(con).getClient()
        homeRepository = HomeRepository(api)
        preferenceProvider = PreferenceProvider(con)

        viewModel = getViewModel()

        viewModel.getFirmEmployees()

        viewModel.resultEmployee.observe(viewLifecycleOwner, Observer {

            var list = ArrayList<EmployeeResponseResultItem>()
            if (it != null) {
                for (i in it) {
                    list.add(i)
                }
                txtEmployeeCount.text = "Total ${list.size} Employees"
            }

            val homeAdapter = HomeAdapter(list, con, con as FragmentClickListener)
            binding.recyclerView.layoutManager = LinearLayoutManager(con)
            binding.recyclerView.adapter = homeAdapter

        })

        binding.drawerIcon.setOnClickListener {
            fragmentClickListener.drawerClick()
        }
    }

    private fun getViewModel(): HomeViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return HomeViewModel(homeRepository, preferenceProvider, con) as T
            }
        })[HomeViewModel::class.java]
    }

    companion object {
        fun newInstance() = HomeFragment()
    }

}