package com.tts.seaa.admin.ui.home

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.tts.seaa.admin.data.preferences.PreferenceProvider
import com.tts.seaa.admin.data.response.employee.*
import com.tts.seaa.admin.data.response.task.*
import com.tts.seaa.admin.utils.Constants
import com.tts.seaa.admin.utils.Coroutines
import com.tts.seaa.admin.utils.NoInternetException

class HomeViewModel(
    private val homeRepository: HomeRepository,
    private val preferenceProvider: PreferenceProvider,
    private val context: Context
) : ViewModel() {

    private var _resultEmployee: MutableLiveData<List<EmployeeResponseResultItem>> = MutableLiveData<List<EmployeeResponseResultItem>>()
    private var _resultEmployeeDetails: MutableLiveData<EmployeeDetailResponse> = MutableLiveData<EmployeeDetailResponse>()
    private var _resultEmployeeTaskList: MutableLiveData<EmployeeTaskListRes> = MutableLiveData<EmployeeTaskListRes>()
    private var _resultEmployeeTask: MutableLiveData<EmployeeTaskResponse> = MutableLiveData<EmployeeTaskResponse>()
    private var _resultEmployeeTaskActivity: MutableLiveData<EmployeeTaskActivityRes> = MutableLiveData<EmployeeTaskActivityRes>()
    private var _resultEmployeeActivity: MutableLiveData<EmployeeActivityResponse> = MutableLiveData<EmployeeActivityResponse>()

    //task
    private var _resultAllTask: MutableLiveData<AllTaskResponse> = MutableLiveData<AllTaskResponse>()
    private var _resultTaskDetail: MutableLiveData<TaskDetailResponse> = MutableLiveData<TaskDetailResponse>()
    private var _resultTaskByDate: MutableLiveData<TaskByDateResponse> = MutableLiveData<TaskByDateResponse>()
    private var _resultTaskTemplate: MutableLiveData<TaskTemplateRes> = MutableLiveData<TaskTemplateRes>()
    private var _resultTaskSub: MutableLiveData<TaskSubRes> = MutableLiveData<TaskSubRes>()

    //Create Task
    private var _resultTeam: MutableLiveData<TeamResponse> = MutableLiveData<TeamResponse>()
    private var _resultCompany: MutableLiveData<CompanyResponse> = MutableLiveData<CompanyResponse>()
    private var _resultTemplate: MutableLiveData<TemplateResponse> = MutableLiveData<TemplateResponse>()
    private var _resultAddTask: MutableLiveData<AddTaskResponse> = MutableLiveData<AddTaskResponse>()


    //Live Data
    var resultEmployee: LiveData<List<EmployeeResponseResultItem>> = _resultEmployee
    var resultEmployeeDetails: LiveData<EmployeeDetailResponse> = _resultEmployeeDetails
    var resultEmployeeTaskList: LiveData<EmployeeTaskListRes> = _resultEmployeeTaskList
    var resultEmployeeTask: LiveData<EmployeeTaskResponse> = _resultEmployeeTask
    var resultEmployeeTaskActivity: LiveData<EmployeeTaskActivityRes> = _resultEmployeeTaskActivity
    var resultEmployeeActivity: LiveData<EmployeeActivityResponse> = _resultEmployeeActivity

    //Task
    var resultAllTask: LiveData<AllTaskResponse> = _resultAllTask
    var resultTaskDetail: LiveData<TaskDetailResponse> = _resultTaskDetail
    var resultTaskByDate: LiveData<TaskByDateResponse> = _resultTaskByDate
    var resultTaskTemplate: LiveData<TaskTemplateRes> = _resultTaskTemplate
    var resultTaskSub: LiveData<TaskSubRes> = _resultTaskSub


    var resultTeam: LiveData<TeamResponse> = _resultTeam

    //Create Task
    var resultCompany: LiveData<CompanyResponse> = _resultCompany
    var resultTemplate: LiveData<TemplateResponse> = _resultTemplate
    var resultAddTask: LiveData<AddTaskResponse> = _resultAddTask

    fun getFirmEmployees() {
        Coroutines.main {
            try {
                val tasks = homeRepository.getEmployee(preferenceProvider.getFirmKeyAsJsonObject())
                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultEmployee.value = tasks.body()!!.result as List<EmployeeResponseResultItem>
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getEmployeeDetails(empId: String) {
        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty("emp_id", empId)
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                val tasks = homeRepository.getEmployeeDetails(jsonObject)

                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultEmployeeDetails.value = tasks.body()
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getEmployeeTaskList(empId: String) {
        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty(Constants.ARG_EMP_ID, empId)
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                val tasks = homeRepository.getEmployeeTaskList(jsonObject)

                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultEmployeeTaskList.value = tasks.body()
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getEmployeeTask(empId: String, task_id: String) {

        Coroutines.main {
            try {

                val jsonObject = JsonObject()
                jsonObject.addProperty(Constants.ARG_EMP_ID, empId)
                jsonObject.addProperty(Constants.ARG_TASK_ID, task_id)
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                val tasks = homeRepository.getEmployeeTask(jsonObject)

                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultEmployeeTask.value = tasks.body()
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getEmployeeTaskActivities(empId: String, task_id: String) {

        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty(Constants.ARG_EMP_ID, empId)
                jsonObject.addProperty(Constants.ARG_TASK_ID, task_id)
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                val tasks = homeRepository.getEmployeeTaskActivities(jsonObject)

                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultEmployeeTaskActivity.value = tasks.body()
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }


    fun getEmployeeActivity(empId: String) {


        Coroutines.main {
            try {

                val jsonObject = JsonObject()
                jsonObject.addProperty(Constants.ARG_EMP_ID, empId)
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                val tasks = homeRepository.getEmployeeActivity(jsonObject)

                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultEmployeeActivity.value = tasks.body()
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getAllTask(employee: String, company: String, status: String) {


        Coroutines.main {
            try {

                val jsonObject = JsonObject()
                jsonObject.addProperty(Constants.TASK_EMPLOYEE, employee)
                jsonObject.addProperty(Constants.TASK_COMPANY, company)
                jsonObject.addProperty(Constants.TASK_STATUS, status)
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())

                val tasks = homeRepository.getAllTasks(jsonObject)
                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultAllTask.value = tasks.body()!!
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }


    fun getTaskByDate(task_date: String) {


        Coroutines.main {
            try {

                val jsonObject = JsonObject()
                jsonObject.addProperty("task_date", task_date)
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                val tasks = homeRepository.getTaskByDate(jsonObject)

                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultTaskByDate.value = tasks.body()
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }


    fun getTeams() {
        Coroutines.main {
            try {
                val tasks = homeRepository.getTeams(preferenceProvider.getFirmKeyAsJsonObject())
                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultTeam.value = tasks.body()!!
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getCompanies() {
        Coroutines.main {
            try {
                val tasks = homeRepository.getCompanies(preferenceProvider.getFirmKeyAsJsonObject())
                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultCompany.value = tasks.body()!!
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getTemplate() {
        Coroutines.main {
            try {
                val tasks = homeRepository.getTemplate(preferenceProvider.getFirmKeyAsJsonObject())
                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultTemplate.value = tasks.body()!!
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun addTask(addTask: JsonObject) {
        Coroutines.main {
            try {
                val tasks = homeRepository.addTask(addTask)
                if (tasks.isSuccessful && tasks.body() != null) {
                    _resultAddTask.value = tasks.body()!!
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getTaskDetails(task_id: String) {
        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty(Constants.ARG_TASK_ID, task_id)
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())

                val tasks = homeRepository.getTaskDetails(jsonObject)
                if (tasks.isSuccessful && tasks.body() != null) {
                    _resultTaskDetail.value = tasks.body()!!
                } else
                    Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getTaskTemplatesActivities(task_id: String, template_id: String) {
        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty(Constants.ARG_TASK_ID, task_id)
                jsonObject.addProperty(Constants.TEMPLATE_ID, template_id)
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())

                val tasks = homeRepository.getTaskTemplatesActivities(jsonObject)
                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultTaskTemplate.value = tasks.body()!!
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            } catch (e: NoInternetException) {
                Toast.makeText(context, "No Internet", Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getTaskSubActivities(task_act_id: String) {
        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty(Constants.TASK_ACT_ID, task_act_id)
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())

                val tasks = homeRepository.getTaskSubActivities(jsonObject)
                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultTaskSub.value = tasks.body()!!
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            } catch (e: NoInternetException) {
                Toast.makeText(context, "No Internet", Toast.LENGTH_SHORT).show()

            }
        }
    }


}