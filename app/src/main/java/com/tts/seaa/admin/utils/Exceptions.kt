package com.tts.seaa.admin.utils

import java.io.IOException

class ApiException(message: String) : IOException(message)
class NoConnectivityException: IOException()
class NoInternetException(message: String) : IOException(message)
class LocationPermissionNotGrantedException: Exception()
class DateNotFoundException: Exception()