package com.tts.seaa.admin.data.response.task

import com.google.gson.annotations.SerializedName

data class TaskSubRes(

	@field:SerializedName("result")
	val result: List<SubTaskResultItem?>? = null,

	@field:SerializedName("key")
	val key: String? = null,
	val message: String? = null

)

data class SubFilesItem(

	@field:SerializedName("sub_act_id")
	val subActId: String? = null,

	@field:SerializedName("sf_id")
	val sfId: String? = null,

	@field:SerializedName("sf_file_tags")
	val sfFileTags: String? = null,

	@field:SerializedName("sf_file_discription")
	val sfFileDiscription: String? = null,

	@field:SerializedName("sf_filename")
	val sfFilename: String? = null,

	@field:SerializedName("sf_date_added")
	val sfDateAdded: String? = null,

	@field:SerializedName("task_act_id")
	val taskActId: String? = null
)

data class SubTaskResultItem(

	@field:SerializedName("sub_act_id")
	val subActId: String? = null,

	@field:SerializedName("sa_act_date")
	val saActDate: String? = null,

	@field:SerializedName("sa_temact_id")
	val saTemactId: String? = null,

	@field:SerializedName("sa_tem_id")
	val saTemId: String? = null,

	@field:SerializedName("firm_id")
	val firmId: String? = null,

	@field:SerializedName("task_act_id")
	val taskActId: String? = null,

	@field:SerializedName("sub_files")
	val subFiles: List<SubFilesItem?>? = null,

	@field:SerializedName("sa_emp_id")
	val saEmpId: String? = null,

	@field:SerializedName("sa_task_id")
	val saTaskId: String? = null,

	@field:SerializedName("sa_comment")
	val saComment: String? = null
)
