package com.tts.seaa.admin.data.response

data class Result(
    val comments: String,
    val company: String,
    val due_date: String,
    val firm_id: String,
    val job_type: String,
    val priority: String,
    val status: String,
    val supervisor: String,
    val task_added_date: String,
    val task_id: String,
    val task_mod_date: String,
    val team: String,
    val template: String
)