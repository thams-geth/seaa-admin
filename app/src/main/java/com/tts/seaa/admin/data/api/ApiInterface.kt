package com.tts.seaa.admin.data.api

import android.renderscript.Allocation
import com.google.gson.JsonObject
import com.tts.seaa.admin.data.response.employee.*
import com.tts.seaa.admin.data.response.firmkey.FirmKeyRes
import com.tts.seaa.admin.data.response.login.LoginRes
import com.tts.seaa.admin.data.response.task.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface ApiInterface {
    //login
//    @FormUrlEncoded
//    @POST
//    fun login(@Field("userid") userid: String, @Field("password") password: String): Call<LoginResponse>

    @POST("seaa-api/mob/fa/firmauth/getfirmKey")
    suspend fun getFirmKey(@Body body: JsonObject): Response<FirmKeyRes>

    @POST("seaa-api/mob/fa/firmauth/firmlogin")
    suspend fun sendMobileNo(@Body body: JsonObject): Response<LoginRes>

    @POST("seaa-api/mob/fa/firmauth/otpauth")
    suspend fun verifyOTP(@Body body: JsonObject): Response<LoginRes>

    @POST("seaa-api/mob/fa/employee/getfirmemployees")
    suspend fun getEmployees(@Body body: JsonObject): Response<EmployeeResponse>

    //Employee----------
    //Employee----------
    //Employee----------

    @POST("seaa-api/mob/fa/employee/getemployeedetail")
    suspend fun getEmployeeDetails(@Body firm_id: JsonObject): Response<EmployeeDetailResponse>

    @POST("seaa-api/mob/fa/employee/getemployeeTasklist")
    suspend fun getEmployeeTaskList(@Body firm_id: JsonObject): Response<EmployeeTaskListRes>

    @POST("seaa-api/mob/fa/employee/getEmployeeTask")
    suspend fun getEmployeeTask(@Body firm_id: JsonObject): Response<EmployeeTaskResponse>

    @POST("seaa-api/mob/fa/employee/getemployeeTaskActivities")
    suspend fun getEmployeeTaskActivities(@Body firm_id: JsonObject): Response<EmployeeTaskActivityRes>

    @POST("seaa-api/mob/fa/employee/getemployeeActivities")
    suspend fun getEmployeeActivities(@Body firm_id: JsonObject): Response<EmployeeActivityResponse>


    //Task----------
    //Task----------
    //Task----------

    @POST("seaa-api/mob/fa/tasks/getAllTasks")
    suspend fun getAllTask(@Body firm_id: JsonObject): Response<AllTaskResponse>

    @POST("seaa-api/mob/fa/tasks/getAllTasks")
    suspend fun getTaskByDate(@Body firm_id: JsonObject): Response<TaskByDateResponse>

    @POST("seaa-api/mob/fa/tasks/getTeams")
    suspend fun getTeams(@Body firm_id: JsonObject): Response<TeamResponse>

    @POST("seaa-api/mob/fa/tasks/getcompanies")
    suspend fun getCompanies(@Body firm_id: JsonObject): Response<CompanyResponse>

    @POST("seaa-api/mob/fa/tasks/getTemplates")
    suspend fun getTemplate(@Body firm_id: JsonObject): Response<TemplateResponse>

    @POST("seaa-api/mob/fa/tasks/addtask")
    suspend fun addTask(@Body addTask: JsonObject): Response<AddTaskResponse>

    //ViewTask
    @POST("seaa-api/mob/fa/tasks/getTaskdetail")
    suspend fun getTaskDetails(@Body body: JsonObject): Response<TaskDetailResponse>


    //gettasktemplatesactivties
    @POST("seaa-api/mob/fa/tasks/gettasktemplatesactivties")
    suspend fun getTaskTemplatesActivities(@Body body: JsonObject): Response<TaskTemplateRes>

    //getTaskSubActivities
    @POST("seaa-api/mob/fa/tasks/gettasksubactivities")
    suspend fun getTaskSubActivities(@Body body: JsonObject): Response<TaskSubRes>


    @Headers("Content-Type: application/json")
    @POST("authenticate")
//    suspend fun login(@Body jsonObject: JsonObject): Call<LoginResponse>


    @GET("casedetails/agreementDetailsPerf")
    suspend fun allRes(@Query("branchID") branchID: Int): Response<Allocation>


    @GET("casedetails/agreementDetailsPerf")
    suspend fun allocation(@Query("branchID") branchID: Int): Call<ResponseBody>

//    @GET("contactrecording/questions")
//    suspend fun questions(): Response<List<Questions>>

//    @GET("api/v1/employees")
//    suspend fun questions(): Callback<ResponseBody>


}