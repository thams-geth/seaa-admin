package com.tts.seaa.admin.data.response.employee

import com.google.gson.annotations.SerializedName

data class EmployeeTaskActivityRes(

	@field:SerializedName("result")
	val result: List<EmployeeTaskActivityResultItem?>? = null,

	@field:SerializedName("key")
	val key: String? = null,
	val message: String? = null

)

data class EmployeeTaskActivityResultItem(

	@field:SerializedName("act_task_comment")
	val actTaskComment: String? = null,

	@field:SerializedName("act_task_id")
	val actTaskId: String? = null,

	@field:SerializedName("due_date")
	val dueDate: String? = null,

	@field:SerializedName("task_title")
	val taskTitle: String? = null,

	@field:SerializedName("act_date_added")
	val actDateAdded: String? = null,

	@field:SerializedName("act_task_status")
	val actTaskStatus: String? = null,

	@field:SerializedName("act_emp_id")
	val actEmpId: String? = null
)
