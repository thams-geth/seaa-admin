package com.tts.seaa.admin.data.response.firmkey

data class FirmResult(
    val refKey: String? = null
)