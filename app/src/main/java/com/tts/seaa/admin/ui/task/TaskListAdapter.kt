package com.tts.seaa.admin.ui.task

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tts.seaa.admin.R
import com.tts.seaa.admin.data.response.task.AllTaskResponseResultItem
import com.tts.seaa.admin.ui.FragmentClickListener
import kotlinx.android.synthetic.main.item_task_list.view.*

class TaskListAdapter(
    private val list: List<AllTaskResponseResultItem>,
    private val context: Context,
    private val fragmentClickListener: FragmentClickListener
) :
    RecyclerView.Adapter<TaskListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_task_list, parent, false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.txtNo.text = "${position.toInt() + 1}"
        holder.itemView.txtTaskName.text = list[position].taskTitle
        holder.itemView.txtEmployeeName.text = list[position].empName
        holder.itemView.txtDueDate.text = list[position].dueDate

        holder.itemView.setOnClickListener {
            fragmentClickListener.onClickWithEmpIdTaskID("ViewTask", list[position].taEmployee!!, list[position].taskId!!)
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

}