package com.tts.seaa.admin.ui.employee

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tts.seaa.admin.R
import com.tts.seaa.admin.data.api.ApiInterface
import com.tts.seaa.admin.data.api.RetrofitClient
import com.tts.seaa.admin.data.preferences.PreferenceProvider
import com.tts.seaa.admin.data.response.employee.EmployeeTaskListResultItem
import com.tts.seaa.admin.databinding.FragmentEmployeeTaskBinding
import com.tts.seaa.admin.ui.FragmentClickListener
import com.tts.seaa.admin.ui.home.HomeRepository
import com.tts.seaa.admin.ui.home.HomeViewModel
import com.tts.seaa.admin.utils.Constants.Companion.ARG_EMP_ID
import kotlinx.android.synthetic.main.fragment_employee_details.*


class EmployeeTaskFragment : Fragment() {

    lateinit var fragmentClickListener: FragmentClickListener

    private var empId: String? = null

    companion object {
        fun newInstance(emp_id: String) = EmployeeTaskFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_EMP_ID, emp_id)
            }
        }
    }

    private lateinit var con: Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        con = context
        fragmentClickListener = con as FragmentClickListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            empId = it.getString(ARG_EMP_ID)
        }
    }

    lateinit var preferenceProvider: PreferenceProvider
    lateinit var homeRepository: HomeRepository
    lateinit var api: ApiInterface
    private lateinit var viewModel: HomeViewModel
    lateinit var binding: FragmentEmployeeTaskBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employee_task, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        api = RetrofitClient(con).getClient()
        homeRepository = HomeRepository(api)
        preferenceProvider = PreferenceProvider(con)

        viewModel = getViewModel()


        viewModel.getFirmEmployees()
        viewModel.getEmployeeTaskList(empId!!)


        binding.ivBack.setOnClickListener {
            fragmentClickListener.onBack()

        }

        binding.drawerIcon.setOnClickListener {
            fragmentClickListener.drawerClick()
        }
        viewModel.resultEmployee.observe(viewLifecycleOwner, Observer {

            if (it != null) {
                for (item in it) {
                    if (empId == item.empAutoId) {
                        txtEmployeeName.text = item.empName
                        txtPendingTask.text = item.empPendingTask.toString()
                        txtCompletedTask.text = item.empCompletedTask.toString()
                        txtTodayTask.text = item.empCurrentTask?.taskTitle?.toString() ?: "No Task"
                    }
                }
            }

        })

        viewModel.resultEmployeeTaskList.observe(viewLifecycleOwner, Observer {

            var list = ArrayList<EmployeeTaskListResultItem>()
            if (it != null) {
                for (i in it.result!!) {
                    list.add(i!!)
                }
            }
            val homeAdapter = EmployeeTaskListAdapter(list, con, con as FragmentClickListener)
            binding.recyclerView.layoutManager = LinearLayoutManager(con)
            binding.recyclerView.adapter = homeAdapter

        })

    }

    private fun getViewModel(): HomeViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return HomeViewModel(homeRepository, preferenceProvider, con) as T
            }
        })[HomeViewModel::class.java]


    }

}