package com.tts.seaa.admin.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.tts.seaa.admin.R
import com.tts.seaa.admin.data.response.employee.EmployeeResponseResultItem
import com.tts.seaa.admin.ui.FragmentClickListener
import kotlinx.android.synthetic.main.item_employee_list.view.*

class HomeAdapter(
    private val list: List<EmployeeResponseResultItem>,
    private val context: Context,
    private val fragmentClickListener: FragmentClickListener
) :
    RecyclerView.Adapter<HomeAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_employee_list, parent, false)
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.txtNo.text = "${position.toInt()+1}"
        holder.itemView.txtEmployeeName.text = list[position].empName
        holder.itemView.txtEmployeeId.text = list[position].employeeId
        holder.itemView.txtOngoingTask.text =
            if (list[position].empCurrentTask?.taskTitle != null) list[position].empCurrentTask?.taskTitle.toString() else "No Task"
        holder.itemView.txtPendingTask.text = list[position].empPendingTask.toString()
        holder.itemView.txtCompletedTask.text = list[position].empCompletedTask.toString()
        holder.itemView.txtTotalPriorityTask.text = list[position].empPriorityTask.toString()

        holder.itemView.imgExpand.isSelected = false

        holder.itemView.setOnClickListener {
            if (holder.itemView.imgExpand.isSelected) {
                holder.itemView.imgExpand.isSelected = false
                holder.itemView.linearDetails.visibility = View.GONE
                holder.itemView.imgExpand.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_chevron_right_24))
            } else {
                holder.itemView.imgExpand.isSelected = true
                holder.itemView.linearDetails.visibility = View.VISIBLE
                holder.itemView.imgExpand.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_keyboard_arrow_down_24))

            }
        }


        holder.itemView.btnViewEmployee.setOnClickListener {
            fragmentClickListener.onClickWithEmpId("EmployeeDetails", list[position].empAutoId!!)
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }


}