package com.tts.seaa.admin.ui.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.tts.seaa.admin.R
import com.tts.seaa.admin.ui.FragmentClickListener
import com.tts.seaa.admin.ui.alert.AlertFragment
import com.tts.seaa.admin.ui.doc.DocFragment
import com.tts.seaa.admin.ui.doc.UploadDocFragment
import com.tts.seaa.admin.ui.employee.EmployeeActivityFragment
import com.tts.seaa.admin.ui.employee.EmployeeDetailsFragment
import com.tts.seaa.admin.ui.employee.EmployeeTaskFragment
import com.tts.seaa.admin.ui.task.CreateTaskFragment
import com.tts.seaa.admin.ui.task.TaskFragment
import com.tts.seaa.admin.ui.task.ViewTaskFragment
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity(), FragmentClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        supportFragmentManager.beginTransaction().replace(R.id.frameLayout, HomeFragment.newInstance(), "Home").commit()

        navigationView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.drawer_home -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, HomeFragment.newInstance(), "Home").commit()
                }
                R.id.drawer_task -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, TaskFragment.newInstance(), "Task").commit()
                }
                R.id.drawer_documents -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, DocFragment.newInstance(), "Doc").commit()
                }
                R.id.drawer_companyProfile -> {
//                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, AlertFragment.newInstance(), "Alert").commit()
                }
                R.id.drawer_feedback -> {
//                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, AlertFragment.newInstance(), "Alert").commit()
                }
                R.id.drawer_logout -> {
//                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, AlertFragment.newInstance(), "Alert").commit()
                }
            }
            drawer.closeDrawer(GravityCompat.START)
            return@setNavigationItemSelectedListener true
        }


        bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, HomeFragment.newInstance(), "Home").commit()
                }
                R.id.task -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, TaskFragment.newInstance(), "Task").commit()
                }
                R.id.doc -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, DocFragment.newInstance(), "Doc").commit()
                }
                R.id.alert -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, AlertFragment.newInstance(), "Alert").commit()
                }
            }
            return@setOnNavigationItemSelectedListener true

        }
    }

    override fun onClick(fragment: String) {
        if (fragment == "AddActivity") {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, CreateTaskFragment.newInstance(), "AddActivity")
                .addToBackStack("Task").commit()
        }

        if (fragment == "UploadDoc") {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, UploadDocFragment.newInstance(), "UploadDoc")
                .addToBackStack("Doc").commit()
        }


    }

    override fun onClickWithEmpId(fragment: String, emp_id: String) {
        if (fragment == "EmployeeDetails") {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, EmployeeDetailsFragment.newInstance(emp_id), "EmployeeDetails")
                .addToBackStack("Home").commit()
        }

        if (fragment == "EmployeeTasks") {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, EmployeeTaskFragment.newInstance(emp_id), "EmployeeTasks")
                .addToBackStack("Home").commit()
        }

        if (fragment == "EmployeeActivity") {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, EmployeeActivityFragment.newInstance(emp_id), "EmployeeActivity")
                .addToBackStack("Home").commit()
        }

    }

    override fun onClickWithEmpIdTaskID(fragment: String, emp_id: String, task_id: String) {
        if (fragment == "ViewTask") {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, ViewTaskFragment.newInstance(emp_id, task_id), "ViewTask")
                .addToBackStack("Home").commit()
        }
    }

    override fun onBack() {
        supportFragmentManager.popBackStack();
    }


    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun drawerClick() {
        drawer.openDrawer(GravityCompat.START)
    }

}
