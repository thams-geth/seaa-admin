package com.tts.seaa.admin.ui.task

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.tts.seaa.admin.R
import com.tts.seaa.admin.data.response.task.TaskTemplateResultItem
import com.tts.seaa.admin.ui.FragmentClickListener
import kotlinx.android.synthetic.main.item_task_template_list.view.*

class TaskTemplateListAdapter(
    private val list: List<TaskTemplateResultItem>,
    private val context: Context,
    private val fragmentClickListener: FragmentClickListener
) :
    RecyclerView.Adapter<TaskTemplateListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_task_template_list, parent, false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.txtNo.text = list[position].tmpActId
        holder.itemView.txtTemplateName.text = list[position].tmpActName
        if (list[position].temActStatus != null && list[position].temActStatus?.actTaskStatus == "1") {
            holder.itemView.txtStatus.text = "Success"
            holder.itemView.txtStatus.setTextColor(ContextCompat.getColor(context, R.color.green))

        } else {
            holder.itemView.txtStatus.text = "Pending"
            holder.itemView.txtStatus.setTextColor(ContextCompat.getColor(context, R.color.yellow))

        }

        holder.itemView.setOnClickListener {
            fragmentClickListener.onClickWithEmpId("EmployeeActivity", list[position].tmpActId!!)
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

}