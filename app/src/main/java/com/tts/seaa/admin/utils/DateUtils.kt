package com.tts.seaa.admin.utils

import java.text.SimpleDateFormat
import java.util.*

fun getTodayDateHeader(): String {
    val sdf = SimpleDateFormat("EEEE , MMM dd", Locale.getDefault())
    return sdf.format(Date())
}