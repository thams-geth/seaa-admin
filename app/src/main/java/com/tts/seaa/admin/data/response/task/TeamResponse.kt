package com.tts.seaa.admin.data.response.task

import com.google.gson.annotations.SerializedName

data class TeamResponse(

    @field:SerializedName("result")
    val result: List<TeamResultItem?>? = null,

    @field:SerializedName("key")
    val key: String? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class TeamResultItem(

    @field:SerializedName("team_status")
    val teamStatus: String? = null,

    @field:SerializedName("work_nature")
    val workNature: String? = null,

    @field:SerializedName("team_lead_name")
    var teamLeadName: String? = null,

    @field:SerializedName("team_id")
    var teamId: String? = null,

    @field:SerializedName("member_count")
    val memberCount: Int? = null,

    @field:SerializedName("team_lead")
    var teamLead: String? = null,

    @field:SerializedName("team_name")
    val teamName: String? = null,

    var isSelected: Boolean? = null

)
