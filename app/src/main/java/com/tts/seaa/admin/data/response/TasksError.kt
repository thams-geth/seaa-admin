package com.tts.seaa.admin.data.response

data class TasksError(
    val id: Int,
    val key: String,
    val message: String
)