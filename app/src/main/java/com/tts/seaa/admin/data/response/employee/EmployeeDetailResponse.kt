package com.tts.seaa.admin.data.response.employee

import com.google.gson.annotations.SerializedName

data class EmployeeDetailResponse(

	@field:SerializedName("result")
	val result: EmployeeDetailResult? = null,

	@field:SerializedName("key")
	val key: String? = null,
	val message: String? = null

)

data class EmployeeDetailResult(

	@field:SerializedName("emp_profilepic")
	val empProfilepic: String? = null,

	@field:SerializedName("emp_dop")
	val empDop: String? = null,

	@field:SerializedName("emp_phone")
	val empPhone: String? = null,

	@field:SerializedName("employee_id")
	val employeeId: String? = null,

	@field:SerializedName("emp_status")
	val empStatus: String? = null,

	@field:SerializedName("emp_name")
	val empName: String? = null,

	@field:SerializedName("emp_address")
	val empAddress: String? = null,

	@field:SerializedName("emp_auto_id")
	val empAutoId: String? = null,

	@field:SerializedName("emp_email")
	val empEmail: String? = null
)
