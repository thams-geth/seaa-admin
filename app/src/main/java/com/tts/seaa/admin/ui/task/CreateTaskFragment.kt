package com.tts.seaa.admin.ui.task

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.tts.seaa.admin.R
import com.tts.seaa.admin.data.api.ApiInterface
import com.tts.seaa.admin.data.api.RetrofitClient
import com.tts.seaa.admin.data.preferences.PreferenceProvider
import com.tts.seaa.admin.data.response.task.CompanyResultItem
import com.tts.seaa.admin.data.response.task.TeamResultItem
import com.tts.seaa.admin.data.response.task.TemplateResultItem
import com.tts.seaa.admin.databinding.FragmentCreateTaskBinding
import com.tts.seaa.admin.ui.FragmentClickListener
import com.tts.seaa.admin.ui.home.HomeRepository
import com.tts.seaa.admin.ui.home.HomeViewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class CreateTaskFragment : Fragment(), AdapterView.OnItemSelectedListener {

    companion object {
        fun newInstance() = CreateTaskFragment()
    }

    lateinit var fragmentClickListener: FragmentClickListener

    private lateinit var con: Context
    private var companyResultItem = ArrayList<CompanyResultItem>()
    private var companyResultItemName = ArrayList<String>()

    private var templateResultItem = ArrayList<TemplateResultItem>()
    private var templateResultItemName = ArrayList<String>()
    private val teamList = ArrayList<TeamResultItem>()


    override fun onAttach(context: Context) {
        super.onAttach(context)
        con = context
        fragmentClickListener = con as FragmentClickListener
    }

    lateinit var preferenceProvider: PreferenceProvider
    lateinit var homeRepository: HomeRepository
    lateinit var api: ApiInterface
    private lateinit var viewModel: HomeViewModel
    lateinit var binding: FragmentCreateTaskBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_task, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        api = RetrofitClient(con).getClient()
        homeRepository = HomeRepository(api)
        preferenceProvider = PreferenceProvider(con)

        viewModel = getViewModel()
        viewModel.getTeams()
        viewModel.getCompanies()
        viewModel.getTemplate()


        viewModel.resultCompany.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                for (i in it.result!!) {
                    companyResultItem.add(i!!)
                    companyResultItemName.add(i.comName!!)
                }
            }
            val ad: ArrayAdapter<String> = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, companyResultItemName)
            ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spnCompany.adapter = ad
            binding.spnCompany.onItemSelectedListener = this
        })

        viewModel.resultTemplate.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                for (i in it.result!!) {
                    templateResultItem.add(i!!)
                    templateResultItemName.add(i.templateName!!)
                }
            }
            val ad: ArrayAdapter<String> = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, templateResultItemName)
            ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spnTemplate.adapter = ad
            binding.spnTemplate.onItemSelectedListener = this
        })


        viewModel.resultTeam.observe(viewLifecycleOwner, Observer {

            var pos = 0;
            if (it != null) {
                for (i in it.result!!) {
                    i!!.isSelected = pos == 0
                    pos = +1
                    teamList.add(i)
                }
            }

            val homeAdapter = TeamListAdapter(teamList, con, con as FragmentClickListener)
            binding.recyclerView.layoutManager = LinearLayoutManager(con)
            binding.recyclerView.adapter = homeAdapter

        })

        viewModel.resultAddTask.observe(viewLifecycleOwner, Observer {
            if (it.message == "success") {
                Toast.makeText(con, "Task Added", Toast.LENGTH_SHORT).show()
                fragmentClickListener.onBack()

            } else
                Toast.makeText(con, "Try Again", Toast.LENGTH_SHORT).show()

        })

        binding.extAssignedDate.setOnClickListener {
            selectDate(it)
        }
        binding.extDueDate.setOnClickListener {
            selectDate(it)
        }

        binding.btnCreateTask.setOnClickListener {

            if (validation()) {
                val teamResult = TeamResultItem()
                for (list in teamList) {
                    if (list.isSelected == true) {
                        teamResult.teamId = list.teamId
                        teamResult.teamLead = list.teamLead
                    }

                }
                var priority: Int = 0;
                when (binding.spnPriority.selectedItem.toString()) {
                    "Low" -> priority = 1
                    "Medium" -> priority = 2
                    "High" -> priority = 3
                }

                var jobType: Int = 0
                when (binding.spnJobType.selectedItem.toString()) {
                    "Internal" -> jobType = 1
                    "External" -> jobType = 2

                }


                val jsonObject = JsonObject()
                jsonObject.addProperty("firm_id", preferenceProvider.getFirmKey())
                jsonObject.addProperty("job_type", jobType.toString())
                jsonObject.addProperty("task_title", binding.extTaskName.text.toString())
                jsonObject.addProperty("company", companyResultItem[binding.spnCompany.selectedItemPosition].comId.toString())
                jsonObject.addProperty("team", teamResult.teamId)
                jsonObject.addProperty("template", templateResultItem[binding.spnTemplate.selectedItemPosition].templateId.toString())
                jsonObject.addProperty("supervisor", teamResult.teamLead)
                jsonObject.addProperty("due_date", binding.extDueDate.text.toString())
                jsonObject.addProperty("priority", priority.toString())
                jsonObject.addProperty("comments", binding.extComment.text.toString())

                viewModel.addTask(jsonObject)

            }
        }


    }

    private fun validation(): Boolean {
        var valid = true
        when {
            binding.extTaskName.text.toString() == "" -> {
                valid = false
                binding.extTaskName.error = "Required"
            }
            binding.extAssignedDate.text.toString() == "" -> {
                valid = false
                binding.extAssignedDate.error = "Required"
            }
            binding.extDueDate.text.toString() == "" -> {
                valid = false
                binding.extDueDate.error = "Required"
            }
            binding.extComment.text.toString() == "" -> {
                valid = false
                binding.extComment.error = "Required"
            }
        }
        return valid
    }

    @SuppressLint("SetTextI18n")
    private fun selectDate(it: View) {
        val editText = it as EditText
        val calendar = Calendar.getInstance()
        val day = calendar[Calendar.DAY_OF_MONTH]
        val month = calendar[Calendar.MONTH]
        val year = calendar[Calendar.YEAR]
        // date picker dialog
        val picker = DatePickerDialog(
            requireActivity(),
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val string = year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth.toString()

                val df: DateFormat = SimpleDateFormat("yyyy-mm-dd")
                var date: Date? = Date()
                date = df.parse(string)
                val df1: DateFormat = SimpleDateFormat("yyyy-mm-dd")
                editText.setText(df1.format(date))
            }, year, month, day
        )
        picker.show()

    }

    private fun getViewModel(): HomeViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return HomeViewModel(homeRepository, preferenceProvider, con) as T
            }
        })[HomeViewModel::class.java]


    }

    fun timepicker() {
        val cldr = Calendar.getInstance()
        val hour = cldr[Calendar.HOUR_OF_DAY]
        val minutes = cldr[Calendar.MINUTE]
        // time picker dialog
        // time picker dialog
        val picker = TimePickerDialog(
            context,
            OnTimeSetListener { tp, sHour, sMinute ->
//                txtTimePicker.setText("$sHour:$sMinute")
            }, hour, minutes, true
        )
        picker.show()
    }


    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

//        if (parent!!.id == R.id.spnCompany) {
//
//        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

}