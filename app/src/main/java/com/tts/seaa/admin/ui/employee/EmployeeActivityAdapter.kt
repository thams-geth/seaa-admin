package com.tts.seaa.admin.ui.employee

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tts.seaa.admin.R
import com.tts.seaa.admin.data.response.task.SubTaskResultItem
import com.tts.seaa.admin.ui.FragmentClickListener
import kotlinx.android.synthetic.main.item_activity_list.view.*

class EmployeeActivityAdapter(
    private val list: List<SubTaskResultItem>,
    private val context: Context,
    private val fragmentClickListener: FragmentClickListener
) :
    RecyclerView.Adapter<EmployeeActivityAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_activity_list, parent, false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.txtTitle.text = list[position].saComment

//        holder.itemView.txtTitle.text = if (list[position].taskTitle != "" && list[position].taskTitle != null) list[position].taskTitle else "No Title"
//        holder.itemView.txtComment.text = if (list[position].actTaskComment != "") list[position].actTaskComment else "No Comments"
        holder.itemView.txtTime.text = list[position].saActDate!!.substring(0, 10) + "\n" + list[position].saActDate!!.substring(11, 16)

        holder.itemView.viewTop.visibility = View.VISIBLE
        holder.itemView.viewTop.visibility = View.VISIBLE

        if (list.size == 1) {
            holder.itemView.viewTop.visibility = View.GONE
            holder.itemView.viewBottom.visibility = View.GONE
        } else if (position == 0 && list.size > 1) {
            holder.itemView.viewTop.visibility = View.GONE
        } else if (position == list.size - 1) {
            holder.itemView.viewBottom.visibility = View.GONE
        }

    }


    override fun getItemCount(): Int {
        return list.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}