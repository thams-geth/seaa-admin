package com.tts.seaa.admin.utils

class Constants {
    companion object {
        const val ARG_EMP_ID = "emp_id"
        const val ARG_TASK_ID = "task_id"
        const val TASK_EMPLOYEE = "task_emp"
        const val TASK_COMPANY = "task_com"
        const val TASK_STATUS = "task_status"
        const val TEMPLATE_ID = "template_id"
        const val TASK_ACT_ID = "task_act_id"

    }
}