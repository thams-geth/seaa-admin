package com.tts.seaa.admin.data.response.employee

import com.google.gson.annotations.SerializedName

data class EmployeeActivityResponse(

    @field:SerializedName("result")
    val result: List<EmployeeTaskActivityResultItem?>? = null,

    @field:SerializedName("key")
    val key: String? = null,

    @field:SerializedName("message")
    val message: String? = null
)


