package com.tts.seaa.admin.data.response.login

data class LoginRes(
	val message: String? = null,
	val key: String? = null
)

