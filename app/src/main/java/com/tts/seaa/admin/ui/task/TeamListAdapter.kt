package com.tts.seaa.admin.ui.task

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.tts.seaa.admin.R
import com.tts.seaa.admin.data.response.task.TeamResultItem
import com.tts.seaa.admin.ui.FragmentClickListener
import kotlinx.android.synthetic.main.item_team_list.view.*

class TeamListAdapter(
    private val list: List<TeamResultItem>,
    private val context: Context,
    private val fragmentClickListener: FragmentClickListener
) :
    RecyclerView.Adapter<TeamListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_team_list, parent, false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.txtNo.text = "${position.toInt() + 1}"
        holder.itemView.txtTeamName.text = list[position].teamName
        holder.itemView.txtLeadName.text = list[position].teamLeadName
        holder.itemView.txtTeamCount.text = list[position].memberCount.toString()
        holder.itemView.checkBox.isChecked = list[position].isSelected!!
        holder.itemView.checkBox.isClickable = false

        holder.itemView.setOnClickListener {
            for (pos in list.indices) {
                list[pos].isSelected = false
            }
            list[position].isSelected = list[position].isSelected != true
            notifyDataSetChanged()

        }
        if (list[position].isSelected == true) {
            holder.itemView.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
            holder.itemView.txtTeamName.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.itemView.txtLeadName.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.itemView.txtTeamCount.setTextColor(ContextCompat.getColor(context, R.color.white))

        } else {
            holder.itemView.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cardGrey))
            holder.itemView.txtTeamName.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text))
            holder.itemView.txtLeadName.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text))
            holder.itemView.txtTeamCount.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text))

        }

    }


    override fun getItemCount(): Int {
        return list.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val checkBox: CheckBox = itemView.checkBox

        fun onBind() {
//            checkBox.setOnCheckedChangeListener()
        }

    }

}