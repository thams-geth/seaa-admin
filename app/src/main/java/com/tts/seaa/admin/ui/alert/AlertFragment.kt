package com.tts.seaa.admin.ui.alert

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.tts.seaa.admin.R
import com.tts.seaa.admin.databinding.FragmentAlertBinding
import com.tts.seaa.admin.ui.FragmentClickListener
import com.tts.seaa.admin.utils.getTodayDateHeader

class AlertFragment : Fragment() {

    lateinit var fragmentClickListener: FragmentClickListener
    private lateinit var binding: FragmentAlertBinding

    companion object {
        fun newInstance() = AlertFragment()
    }

    private lateinit var viewModel: AlertViewModel

    private lateinit var con: Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        con = context
        fragmentClickListener = con as FragmentClickListener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_alert, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(AlertViewModel::class.java)



        binding.drawerIcon.setOnClickListener {
            fragmentClickListener.drawerClick()
        }
    }

}