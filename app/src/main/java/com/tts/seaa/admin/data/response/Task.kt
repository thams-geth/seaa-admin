package com.tts.seaa.admin.data.response

data class Task(
    val key: String,
    val result: Result
)