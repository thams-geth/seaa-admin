package com.tts.seaa.admin.data.response.employee

import com.google.gson.annotations.SerializedName

data class EmployeeTaskResponse(

	@field:SerializedName("result")
	val result: EmployeeTaskResult? = null,

	@field:SerializedName("key")
	val key: String? = null,
	val message: String? = null

)

data class EmployeeTaskResult(

	@field:SerializedName("ta_due_date")
	val taDueDate: String? = null,

	@field:SerializedName("assigned_date")
	val assignedDate: String? = null,

	@field:SerializedName("ta_status")
	val taStatus: String? = null,

	@field:SerializedName("task_title")
	val taskTitle: String? = null,

	@field:SerializedName("task_id")
	val taskId: String? = null,

	@field:SerializedName("ta_employee")
	val taEmployee: String? = null,

	@field:SerializedName("ta_completed_date")
	val taCompletedDate: String? = null,

	@field:SerializedName("team_name")
	val teamName: String? = null,

	@field:SerializedName("com_name")
	val comName: String? = null,

	val template: String? = null

)
