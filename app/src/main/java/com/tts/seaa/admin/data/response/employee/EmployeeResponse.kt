package com.tts.seaa.admin.data.response.employee

import com.google.gson.annotations.SerializedName

data class EmployeeResponse(

    @field:SerializedName("result")
    val result: List<EmployeeResponseResultItem?>? = null,

    @field:SerializedName("key")
    val key: String? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class EmployeeResponseResultItem(

    @field:SerializedName("emp_key")
    val empKey: String? = null,

    @field:SerializedName("emp_completed_task")
    val empCompletedTask: Int? = null,

    @field:SerializedName("employee_id")
    val employeeId: String? = null,

    @field:SerializedName("emp_status")
    val empStatus: String? = null,

    @field:SerializedName("emp_name")
    val empName: String? = null,

    @field:SerializedName("emp_priority_task")
    val empPriorityTask: Int? = null,

    @field:SerializedName("emp_today_task")
    val empTodayTask: Int? = null,

    @field:SerializedName("emp_current_task")
    val empCurrentTask: EmpCurrentTask? = null,

    @field:SerializedName("emp_auto_id")
    val empAutoId: String? = null,

    @field:SerializedName("firm_id")
    val firmId: String? = null,

    @field:SerializedName("emp_pending_task")
    val empPendingTask: Int? = null
)

data class EmpCurrentTask(

    @field:SerializedName("ta_due_date")
    val taDueDate: String? = null,

    @field:SerializedName("task_title")
    val taskTitle: String? = null
)
