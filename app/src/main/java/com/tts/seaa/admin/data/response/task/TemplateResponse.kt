package com.tts.seaa.admin.data.response.task

import com.google.gson.annotations.SerializedName

data class TemplateResponse(

    @field:SerializedName("result")
    val result: List<TemplateResultItem?>? = null,

    @field:SerializedName("key")
    val key: String? = null,
    @field:SerializedName("message")
    val message: String? = null
)

data class TemplateResultItem(

    @field:SerializedName("template_name")
    val templateName: String? = null,

    @field:SerializedName("template_status")
    val templateStatus: String? = null,

    @field:SerializedName("template_added")
    val templateAdded: String? = null,

    @field:SerializedName("template_id")
    val templateId: String? = null,

    @field:SerializedName("firm_id")
    val firmId: String? = null
)
