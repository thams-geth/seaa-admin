package com.tts.seaa.admin.ui.doc

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.tts.seaa.admin.R
import com.tts.seaa.admin.databinding.FragmentUploadDocBinding
import com.tts.seaa.admin.utils.getTodayDateHeader

class UploadDocFragment : Fragment() {

    companion object {
        fun newInstance() = UploadDocFragment()
    }

    private lateinit var viewModel: UploadDocViewModel
    private lateinit var binding: FragmentUploadDocBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_upload_doc, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(UploadDocViewModel::class.java)

    }

}