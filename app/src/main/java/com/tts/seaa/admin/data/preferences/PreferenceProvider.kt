package com.tts.seaa.admin.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.google.gson.JsonObject


private const val FIRM_KEY = "firm_key"
private const val MOBILE_KEY = "firm_mobile"


class PreferenceProvider(context: Context) {

    private val appContext = context.applicationContext

    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)

    fun saveFirmKey(token: String) {
        preference.edit().putString(FIRM_KEY, token).apply()
    }

    fun getFirmKey(): String? {
        return preference.getString(FIRM_KEY, null)
    }

    fun saveMobileNo(firm_mobile: String) {
        preference.edit().putString(MOBILE_KEY, firm_mobile).apply()
    }

    fun getMobileNo(): String? {
        return preference.getString(MOBILE_KEY, null)
    }

    fun getFirmKeyAsJsonObject(): JsonObject {
        val jsonObject = JsonObject()
        jsonObject.addProperty("firm_key", getFirmKey())
        return jsonObject
    }


}